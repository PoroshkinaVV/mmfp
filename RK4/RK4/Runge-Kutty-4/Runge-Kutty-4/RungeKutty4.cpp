// RungeKutty4.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <cmath>
using namespace std;


double func1(double x, double y, double z)
{
	return -1000 * y + 999 * z;
}

double func2(double x, double y, double z)
{
	return y - 2 * z;
}

int main()
{
	// Задаем границы отрезка
	//const double a = 0;
	//const double b = 0.01;

	double y0 = 10.0;
	double z0 = 20.0;
	const int N = 100;// задаем число точек на отрезке
	double x[N + 1], y[N + 1], z[N + 1];
	cout << "Vvedite Eps = ";//0.01
	double Eps;
	cin >> Eps;
	cout << "Vvedite shag h = ";//0.0001
	double h;
	cin >> h;
	//double h = (b - a) / N;
	double k1y, k1z, k2y, k2z, k3y, k3z, k4y, k4z, yn, zn;

	x[0] = 0;
	y[0] = y0;
	z[0] = z0;
	for (int i = 0; i < N; i++)
	{

		while (true)
		{

			x[i + 1] = x[i] + h;
			k1y = func1(x[i + 1], y[i + 1], z[i + 1]);
			k1z = func2(x[i + 1], y[i + 1], z[i + 1]);
			k2y = func1(x[i + 1] - h / 2, y[i + 1] - h / 2 * k1y, z[i + 1] - h / 2 * k1z);
			k2z = func2(x[i + 1] - h / 2, y[i + 1] - h / 2 * k1y, z[i + 1] - h / 2 * k1z);
			k3y = func1(x[i + 1] - h / 2, y[i + 1] - h / 2 * k2y, z[i + 1] - h / 2 * k2z);
			k3z = func2(x[i + 1] - h / 2, y[i + 1] - h / 2 * k2y, z[i + 1] - h / 2 * k2z);
			k4y = func1(x[i], y[i + 1] - h*k3y, z[i + 1] - h*k3z);
			k4z = func2(x[i], y[i + 1] - h*k3y, z[i + 1] - h*k3z);
			yn = y[i + 1];
			zn = z[i + 1];
			y[i + 1] = y[i] + h / 6 * (k1y + 2 * k2y + 2 * k3y + k4y);
			z[i + 1] = z[i] + h / 6 * (k1z + 2 * k2z + 2 * k3z + k4z);

			if (fabs(yn - y[i + 1]) < Eps && fabs(zn - z[i + 1]) < Eps)
			{
				break;
			}

		}
	}

	cout << "x  \t y  \t z" << endl;
	cout << x[0] << '\t' << y[0] << '\t' << z[0] << endl;

	for (int i = 0; i < N; i++)
	{
		cout << x[i + 1] << '\t' << y[i + 1] << '\t' << z[i + 1] << endl;
	}

	system("pause");
	return 0;
}

//На другом примере с тем же алгоритмом //Ответ x11 = -0,9 x22 = 0,4
/*
#include "stdafx.h"
#include <iostream>
#include <cmath>
using namespace std;


double func1(double x, double y, double z)
{
	return -52 * y - 100 * z + exp(-x);
}

double func2(double x, double y, double z)
{
	return y + sin(x);
}

int main()
{
	// Задаем границы отрезка
	//const double a = 0;
	//const double b = 0.01;

	double y0 = 1.0;
	double z0 = 0.0;
	const int N = 200;// задаем число точек на отрезке
	double x[N + 1], y[N + 1], z[N + 1];
	cout << "Vvedite Eps = ";//0.01
	double Eps;
	cin >> Eps;
	cout << "Vvedite shag h = ";//0.01
	double h;
	cin >> h;
	//double h = (b - a) / N;
	double k1y, k1z, k2y, k2z, k3y, k3z, k4y, k4z, yn, zn;

	x[0] = 0;
	y[0] = y0;
	z[0] = z0;
	for (int i = 0; i < N; i++)
	{

		while (true)
		{

			x[i + 1] = x[i] + h;
			k1y = func1(x[i + 1], y[i + 1], z[i + 1]);
			k1z = func2(x[i + 1], y[i + 1], z[i + 1]);
			k2y = func1(x[i + 1] - h / 2, y[i + 1] - h / 2 * k1y, z[i + 1] - h / 2 * k1z);
			k2z = func2(x[i + 1] - h / 2, y[i + 1] - h / 2 * k1y, z[i + 1] - h / 2 * k1z);
			k3y = func1(x[i + 1] - h / 2, y[i + 1] - h / 2 * k2y, z[i + 1] - h / 2 * k2z);
			k3z = func2(x[i + 1] - h / 2, y[i + 1] - h / 2 * k2y, z[i + 1] - h / 2 * k2z);
			k4y = func1(x[i], y[i + 1] - h*k3y, z[i + 1] - h*k3z);
			k4z = func2(x[i], y[i + 1] - h*k3y, z[i + 1] - h*k3z);
			yn = y[i + 1];
			zn = z[i + 1];
			y[i + 1] = y[i] + h / 6 * (k1y + 2 * k2y + 2 * k3y + k4y);
			z[i + 1] = z[i] + h / 6 * (k1z + 2 * k2z + 2 * k3z + k4z);

			if (fabs(yn - y[i + 1]) < Eps && fabs(zn - z[i + 1]) < Eps)
			{
				break;
			}

		}
	}

	cout << "x  \t y  \t z" << endl;
	cout << x[0] << '\t' << y[0] << '\t' << z[0] << endl;

	for (int i = 0; i < N; i++)
	{
		cout << x[i + 1] << '\t' << y[i + 1] << '\t' << z[i + 1] << endl;
	}

	system("pause");
	return 0;
}
*/