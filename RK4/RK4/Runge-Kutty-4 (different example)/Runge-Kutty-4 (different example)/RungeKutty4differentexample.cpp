// RungeKutty4differentexample.cpp: определяет точку входа для консольного приложения.
//
/*
//Ответ должен быть x11 = -0,9 x22 = 0,4
#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

double f1(double x1, double x2, double t) // первая функция 
{
	return -52 * x1 - 100 * x2 + exp(-t);
}

double f2(double x1, double t) // вторая функция
{
	return x1 + sin(t);
}

int main()
{
	//setlocale(LC_ALL,"rus");

	double h = 0.01;      // шаг

	double x1 = 1.0;
	double x2 = 0.0;
	double t = 0.0;
	cout << setw(10) << t << setw(12) << x1 << setw(12) << x2 << endl;

	while (t <= 2.0)
	{

		double k1 = h*f1(x1, x2, t);
		double m1 = h*f2(x1, t);

		double k2 = h*f1(x1 + k1 / 2, x2 + m1 / 2, t + h / 2);
		double m2 = h*f2(x1 + k1 / 2, t + h / 2);

		double k3 = h*f1(x1 + k2 / 2, x2 + m2 / 2, t + h / 2);
		double m3 = h*f2(x1 + k2 / 2, t + h / 2);

		double k4 = h*f1(x1 + k3, x2 + m3, t + h);
		double m4 = h*f2(x1 + k2, t + h);

		x1 += (k1 + 2 * k2 + 2 * k3 + k4) / 6;
		x2 += (m1 + 2 * m2 + 2 * m3 + m4) / 6;
		t += h;

		cout << setw(10) << t << setw(12) << x1 << setw(12) << x2 << endl;

	}

	system("pause");
	return 0;
}
*/
//Этот алгоритм для нашего примера

#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

double f1(double x1, double x2, double t) // первая функция 
{
	return -1000 * x1 - 999 * x2;
}

double f2(double x1, double x2, double t) // вторая функция
{
	return x1 - 2 * x2;
}

int main()
{
	//setlocale(LC_ALL,"rus");

	double h = 0.001;      // шаг

	double x1 = 10.0;
	double x2 = 20.0;
	double t = 0.0;
	cout << setw(10) << t << setw(12) << x1 << setw(12) << x2 << endl;

	while (t <= 0.02)
	{

		double k1 = h*f1(x1, x2, t);
		double m1 = h*f2(x1, x2, t);

		double k2 = h*f1(x1 + k1 / 2, x2 + m1 / 2, t + h / 2);
		double m2 = h*f2(x1 + k1 / 2, x2 + m1 / 2, t + h / 2);

		double k3 = h*f1(x1 + k2 / 2, x2 + m2 / 2, t + h / 2);
		double m3 = h*f2(x1 + k2 / 2, x2 + m2 / 2, t + h / 2);

		double k4 = h*f1(x1 + k3, x2 + m3, t + h);
		double m4 = h*f2(x1 + k2, x2 + m3, t + h);

		x1 += (k1 + 2 * k2 + 2 * k3 + k4) / 6;
		x2 += (m1 + 2 * m2 + 2 * m3 + m4) / 6;
		t += h;

		cout << setw(10) << t << setw(12) << x1 << setw(12) << x2 << endl;

	}

	system("pause");
	return 0;
}