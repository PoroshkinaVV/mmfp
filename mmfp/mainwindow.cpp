#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "task.h"
#include "taskdialog.h"
#include "resultsdialog.h"
#include "qcustomplot.h"
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QPushButton>
#include <QDomDocument>
#include <QList>
#include <QRadioButton>
#include <QGroupBox>
#include <QDebug>
#include <QProgressBar>
#include <QTime>
#include <QTextCodec>
#include <iostream>
#include <fstream>
#include <cmath>
#include <QGraphicsScene>
#include <QGraphicsView>

using namespace std;

int ANS_COUNT = 5; //максимальное число вариантов
int TASK_COUNT = 10; // всего вопросов в тесте

QString MainWindow::getCurrentTimeF()
{
    return  QString("[" + QDateTime().currentDateTime().toString() + "]");
}

QList<int> getRandomIntVector(int start, int countOfNumbers, int length)
{
    qsrand (QDateTime::currentMSecsSinceEpoch());
    QList<int> numbers;
    QList<int> returnNumbers;
    for (int i = start; i < countOfNumbers; i++)
        numbers.append(i);
    for (int i = 0; i < length; i++)
    {
        int num = qrand() % numbers.count();
        returnNumbers.append(numbers.at(num));
        numbers.removeAt(num);
    }
    return returnNumbers;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QFont buttonFont = QFont("Times", 16, QFont::Bold);
    QSize buttonSize = QSize(50,50);


    ui->pushButton->setFont(buttonFont);
    ui->pushButton->setIcon(QIcon("://icons/lec.png"));
    ui->pushButton->setIconSize(buttonSize);
    ui->pushButton_2->setFont(buttonFont);
    ui->pushButton_2->setIcon(QIcon("://icons/mod.png"));
    ui->pushButton_2->setIconSize(buttonSize);
    ui->pushButton_3->setFont(buttonFont);
    ui->pushButton_3->setIcon(QIcon("://icons/die.png"));
    ui->pushButton_3->setIconSize(buttonSize);
    ui->pushButton_4->setFont(buttonFont);
    ui->pushButton_4->setIcon(QIcon("://icons/conf.png"));
    ui->pushButton_4->setIconSize(buttonSize);


    QFile file("://lectures/test.html");
    if (!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::information(this, "Error Lectures", file.errorString());
    }
    else
    {
        QString html(file.readAll());
    }
    QGraphicsPixmapItem *logo = new QGraphicsPixmapItem();
    QGraphicsScene *logo_scene = new QGraphicsScene();
    QPixmap logo_bmp;

    if(!logo_bmp.load("://lectures/big.png", "PNG"));
    logo->setPixmap(logo_bmp);
    logo_scene->addItem(logo);

    logo_scene->addItem(logo);

    ui->graphicsView->setScene(logo_scene);
    ui->stackedWidget->setCurrentWidget(ui->page);

    prepareTestGui();
}
//создает гуи для тестов
void MainWindow::prepareTestGui()
{
    QFont reg_button_font = QFont("Times", 12);
    ui->taskPlainText->setReadOnly(true);
    ui->taskPlainText->zoomIn(2);
    //вопрос
//    m_questLabel = new QLabel("Вопрос");
//    m_questLabel->hide();
//    ui->vanswersLayout->addWidget(m_questLabel);

    //ответы
    for(int i = 0; i < ANS_COUNT; i++)
    {
        QRadioButton* answer = new QRadioButton();
        QHBoxLayout *anshLayout = new QHBoxLayout();
        ui->vanswersLayout->addLayout(anshLayout);
        anshLayout->addStretch();
        anshLayout->addWidget(answer);
        anshLayout->addStretch();
        answer->hide();
        m_answersList.append(answer);
        connect(answer, SIGNAL(clicked()), SLOT(checkAnswerCorrectness()));
    }

    //ответ в текстовую строку
    m_lineAnswer = new QLineEdit();
    m_lineAnswer->hide();
    ui->hansLineLayout->addWidget(m_lineAnswer);
    QSpacerItem *vSpacer = new QSpacerItem(200, 0);
    ui->hansLineLayout->addSpacerItem(vSpacer);

    //кнопка назад
    m_prevButton = new QPushButton();
    m_prevButton ->setFont(reg_button_font);
    m_prevButton->setText("Предыдущий");
    m_prevButton->setDisabled(true);
    ui->hcontrolLayout->addWidget(m_prevButton);
    connect(m_prevButton, SIGNAL(clicked()), SLOT(prevTaskLoad()));

    //прогресс бар
    ui->hcontrolLayout->addStretch();
    m_completenessBar = new QProgressBar();
    m_completenessBar->setMaximum(TASK_COUNT);
    m_completenessBar->setMinimum(0);
    m_completenessBar->setValue(0);
    m_completenessBar->setTextVisible(true);
    m_completenessBar->setAlignment(Qt::AlignCenter);
    m_completenessBar->setFormat(QString("Ответов %1/%2").arg(m_completenessBar->value()).arg(TASK_COUNT));
    ui->hcontrolLayout->addWidget(m_completenessBar);
    ui->hcontrolLayout->addStretch();

    //кнопка вперед
    m_nextButton = new QPushButton();
    m_nextButton ->setFont(reg_button_font);
    m_nextButton->setText("Следующий");
    m_nextButton->setDisabled(true);
    ui->hcontrolLayout->addWidget(m_nextButton);
    connect(m_nextButton, SIGNAL(clicked()), SLOT(nextTaskLoad()));
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::setPage(int index)
{
    if(ui->stackedWidget->currentIndex() == 3)
    {
        QMessageBox mb(0 , tr("Все несохраненные изменения \nбудут потеряны. Продолжить?"),
                             QMessageBox::Warning,
                             QMessageBox::Yes,
                             QMessageBox::No | QMessageBox::Default | QMessageBox::Escape ,
                             QMessageBox::NoButton  );
            mb.setButtonText(QMessageBox::Yes, tr("Да"));
            mb.setButtonText(QMessageBox::No, tr("Отмена"));
            QPixmap exportSuccess("://icons/main.png");
            mb.setWindowIcon(QIcon(exportSuccess));
            if( mb.exec() == QMessageBox::Yes ) {

            } else {
              return;
            }
    }
    if (ui->stackedWidget->currentIndex() == 2 && isTestStarted && index != 4)
    {
        return;
    }

    switch (index) {
    case 1:
        ui->stackedWidget->setCurrentWidget(ui->page);
        break;
    case 2:
        ui->stackedWidget->setCurrentWidget(ui->page_2);
        break;
    case 3:
        if(!isTestStarted)
        {
            auth_dialog = new authdialog(this);
            auth_dialog->setStatusTest(this);
            connect(auth_dialog, &authdialog::showTest, this, &MainWindow::startTest);
            auth_dialog->show();
        }
        else ui->stackedWidget->setCurrentWidget(ui->page_3);
        break;
    case 4:
        ui->stackedWidget->setCurrentWidget(ui->page_4);
        isTestStarted = false;
        break;
    default:
        return;
        break;
    }
}

void MainWindow::startTest()
{
    isTestStarted = true;
    setPage(3);
}

void MainWindow::setStudentName(QString studentName)
{
    m_studentName = studentName;
    m_log += getCurrentTimeF() + "Начало тестирования. Студент " + m_studentName + "\r\n";
}

void MainWindow::on_pushButton_clicked()
{
    authorized = false;
    setPage(1);
}

void MainWindow::on_pushButton_2_clicked()
{
    authorized = false;
    setPage(2);
}

double func1(double a, double b, double x, double y, double z)
{
    return a * y + b * z;
}

double func2(double a, double b, double x, double y, double z)
{
    return a * y - b * z;
}


        void MainWindow::on_pushButton_5_clicked()
        {

            QVector<double> T_i, A_i, w_i, w_i_i, time, n, n_n_0;
               double maxy, miny, h, max;
               int i_group;//количество групп
               double  p, k_eff;//значение реактивности,эффективный коэффициент размножения нейтронов, p
               double lm;//время жизни мгновенных нейтронов, с
               static	const  double beta_i[6] = {0.00021, 0.00140, 0.00126, 0.00252, 0.00074, 0.00027};
               double beta = 0.0064;
               static	const  double lambda_i[6] = {0.0124, 0.0305, 0.1114, 0.3013, 1.136, 3.013};
               double lambda = 0.077;//постоянная распада, с^-1


               QString S1 = ui->lineEdit->text();
               i_group = S1.toDouble();
               if (i_group!=1 && i_group!=2 && i_group!=3 && i_group!=4 && i_group!=5 && i_group!=6)
               {
                   QMessageBox::warning(this,tr("Ошибка"),tr("Количество групп задано не верно"));
                   return;
               }


               QString S2 = ui->lineEdit_2->text();
               p = S2.toDouble();

               if (p > 0.1)
               {
                   QMessageBox::warning(this,tr("Предупреждение"),
                                        tr("Слишком большое по модулю значение реактивности"));
                   return;
               }

               if (p < -0.1)
               {
                   QMessageBox::warning(this,tr("Предупреждение"),
                                        tr("Слишком большое по модулю значение реактивности"));
                   return;
               }


               QString S3 = ui->lineEdit_3->text();
               lm = S3.toDouble();

               if (lm < 0)
               {
                   QMessageBox::warning(this,tr("Ошибка"),
                                        tr("Значение времени жизни мгновенных нейтронов отрицательное"));
                   return;
               }

               if (lm > 0.1)
               {
                   QMessageBox::warning(this,tr("Ошибка"),
                                        tr("Значение времени жизни мгновенных нейтронов слишком большое"));
                   return;
               }

               QString S4 = ui->lineEdit_4->text();
               max = S4.toDouble();

               QString S5 = ui->lineEdit_5->text();
               h = S5.toDouble();


               if ( max/h > 1000000)
               {
                   QMessageBox::warning(this,tr("Предупреждение"),
                                        tr("Задайте другой шаг или количество точек"));
                   return;
               }

               if ((p > 0) && (p <= 0.003) && i_group<3)
               {
                   maxy = 1.5;
                   miny = 1;
               }
               if ((p > 0.003) && i_group<=3)
               {
                   maxy = 2;
                   miny = 1;
               }
               if ((p >= 0.001) && i_group>=3)
               {
                   maxy = 4;
                   miny = 1;
               }
               if ((p > 0) && (p < 0.001) && i_group>3)
               {
                   maxy = 1.5;
                   miny = 1;
               }
               if (p < 0)
               {
                   maxy = 1;
                   miny = 0;
               }


               k_eff = 1/(1-p);//связь реактивности и эффективного коэффициента размножения нейтронов

               double sum = 0;
               double p_w = 0;

               double x=0, y=0;
               double w = 0;


             w_i.append((lambda*p)/(beta-p+lambda*lm));//w_0
             A_i.append(beta/(beta - p));//A_0

             for (double w = -9; w >= -40; w -= 3)
               {

                   p_w = 0;
                   sum = 0;


                   for (int j = 0; j < 6; j++)
                   {
                       sum += (w * beta_i[j])/(w + lambda_i[j]);

                   }

                   p_w = ((w * lm)/(w * lm +1)) + (1/(w * lm +1)) * sum;

                   if ((p_w > p - 0.05) && (p_w < p + 0.01))// && (w==qRound(w)) ((p_w > p - 0.005) && (p_w < p + 0.001))
                   {
                      w_i.append(w);

                   }

              }


               double size_wi;
               size_wi = w_i.size();


               if (size_wi < 7)
               {
                   int z = 7 - size_wi;
                   for ( int r=1; r <= z; r++)
                   {
                      w_i.append(w_i[size_wi-1]);
                   }

               }


               for (int k=1; k < 7; k++)
               {


                    x+=(lambda_i[k-1]*beta_i[k-1]*(1/w_i[k])*(1/w_i[k]))/((1+lambda_i[k-1]/w_i[k])*(1+lambda_i[k-1]/w_i[k]));

               }
               for (int q = 1; q < 7; q++)
               {


                  A_i.append(p*(1/w_i[q])/(lm + x));
               }



               for (double ti = 0; ti < max; ti+=h)
                {
                   y = 0;
                   for (int j = 0; j <= i_group; j++)
                   {
                       y+= A_i[j]*exp(w_i[j]*ti);
                   }
                   time.append(ti/100);

                   n.append(y);

                   n_n_0.append(n[ti]/n[0]);


                }

        QString S6 = ui->lineEdit_6->text();
        double max_y = S6.toDouble();
        QString S7 = ui->lineEdit_7->text();
        double max_x = S7.toDouble();
        //Строим график

        ui->widget->clearGraphs();//Очищаем графики
        //Добавляем один график в widjet
        ui->widget->addGraph();
        //Говорим, что отрисовать нужно график по двум нашим массивам
        ui->widget->graph(0)->setData(time, n_n_0);



        //Подписываем оси 0x и 0y
        ui->widget->xAxis->setLabel("time");
        ui->widget->yAxis->setLabel("n/n_0");

        //Установим область, которая будет показываться на графике

        ui->widget->xAxis->setRange(0, max/100 - 7);//Для оси Ox
        ui->widget->yAxis->setRange(miny, maxy);//Для оси Oy
        if (max_x || max_y)
            {
                ui->widget->xAxis->setRange(0, max_x);//Для оси Ox
                ui->widget->yAxis->setRange(0, max_y);//Для оси Oy
            }


        //И перерисуем график на нашем widjet
        ui->widget->replot();//Чтобы любые изменения в графике появлялись на экране


    }




void MainWindow::on_pushButton_3_clicked()
{
    authorized = false;
    if(isTestStarted)
        return;
    //открываем файл с тестами и обрабатываем
    QFile file("lib/my.tests");
    if (!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::information(this, "Невозможно открыть файл с тестами", file.errorString());
        return;
    }    
    else
    {
        QString reads =  decodeStr(QString(file.readAll()));
        qDebug() << reads;
        testsDom.setContent(reads);
        file.close();
    }

    currentTask = 0;
    buildTest();
    createTaskGui();

    setPage(3);
}

void MainWindow::on_pushButton_4_clicked()
{
    if (authorized || ui->stackedWidget->currentIndex() == 3){
        setSecretChapterIsVisible();
    }
    else{
        auth_dialog = new authdialog(this);
        connect(auth_dialog, &authdialog::showSecretChapter, this, &MainWindow::setSecretChapterIsVisible);
        auth_dialog->show();
    }
}


void MainWindow::buildTest()
{

    QString question;
    QList<QString> answers;
    int rightAnswer;
    QString difficulty;
    m_easyTasks.clear();
    m_mediumTasks.clear();
    m_difficultTasks.clear();
    m_themes.clear();
    m_tasks.clear();
    m_correctness.clear();
    m_selectedAnswers.clear();
    m_completenessBar->setValue(0);
    m_completenessBar->setFormat(QString("Ответов %1/%2").arg(m_completenessBar->value()).arg(TASK_COUNT));

    QDomNodeList themesList = testsDom.elementsByTagName("theme");
    QDomNode themeNode;
    QString theme;
    QDomNodeList partsList;
    for (int i = 0; i < themesList.count(); i++)
    {

       theme = themesList.at(i).toElement().attribute("name");
       partsList = themesList.at(i).toElement().elementsByTagName("part");

       for (int j = 0; j < partsList.count(); j++)
       {
           answers.clear();
           QDomElement elem = partsList.at(j).toElement();
           QDomNodeList qAa = elem.childNodes();
           question = qAa.at(0).toElement().text();
           qDebug() << question << " " << qAa.count();
           for (int k = 1; k < qAa.count(); k++)//because first was answer
           {
               if (qAa.at(k).toElement().attribute("correct").contains("true"))
                   rightAnswer = k-1;
               answers.append(qAa.at(k).toElement().text());
           }
           difficulty = elem.attribute("difficulty");

           QList<int> _randNumbers = getRandomIntVector(0, answers.count(), answers.count());
           rightAnswer = _randNumbers.indexOf(rightAnswer);
           qDebug() << _randNumbers;
           QList<QString> _buf = answers;
           answers.clear();//before shuffling
           for(int ind = 0; ind < _buf.count(); ind++)
           {
               answers.append(_buf.at(_randNumbers.at(ind)));
           }

           Task newTask(question, answers, rightAnswer, difficulty, theme);
           if(difficulty == "easy")
           {
               m_easyTasks.append(newTask);
           }
           else if( difficulty == "medium")
           {
               m_mediumTasks.append(newTask);
           }
           else
           {
               m_difficultTasks.append(newTask);
           }
       }
    }
    qDebug() << "Успешно распарсили";

    shuffleTasks(m_mediumTasks, 4);
    shuffleTasks(m_difficultTasks, 2);
    shuffleTasks(m_easyTasks, 4);
    qDebug() << "Успешно расшафлили";

    if(m_mediumTasks.count() < 4 || m_easyTasks.count() < 4 || m_difficultTasks.count() < 2)
    {
        QMessageBox::information(this ,"Внимание", "Количество заданий недостаточно для формирования теста\n (нужно 4 легких, 4 средний, 2 тяжелых)");
        return;
    }

    QList<int> randNumbers = getRandomIntVector(0, TASK_COUNT, TASK_COUNT);
    QList<Task> buf = m_tasks;
    m_tasks.clear();//before shuffling
    for(int i = 0; i < TASK_COUNT; i++)
    {
        m_tasks.append(buf.at(randNumbers.at(i)));
    }

}

void MainWindow::shuffleTasks(QList<Task> tasksList, int COUNT)
{
    int count = tasksList.count();
    QList<int> randNumbers;
    if(count < COUNT)
    {
         randNumbers  = getRandomIntVector(0, count, count);
    }
    else
    {
        randNumbers  = getRandomIntVector(0, count, COUNT);
    }
    for(int i = 0; i < randNumbers.count(); i++)
    {
        m_tasks.append(tasksList.at(randNumbers.at(i)));
    }
}

//скрывание всех элементов гуи для обновления к следующему заданию
void MainWindow::hideTestGui()
{
    ui->taskPlainText->clear();
    for(int i = 0; i < ANS_COUNT; i++)
    {
        m_answersList.at(i)->setChecked(true);
        m_answersList.at(i)->hide();
    }
    //select previus answers if need
    if(m_selectedAnswers.contains(currentTask)){
        m_answersList.at(m_selectedAnswers[currentTask])->setChecked(true);
    }

    m_lineAnswer->hide();
    m_prevButton->setDisabled(true);
    m_nextButton->setDisabled(true);
}

QString MainWindow::createHtml()
{
    QString str;
    str.append(QString("<center><h2>Вопрос №") + QString::number(currentTask+1) + QString("</center></h2>"));
    str.append(QString("<p><i>") + m_tasks.at(currentTask).getQuestion() + QString("</i></p>"));
    QList<QString> answers = m_tasks.at(currentTask).getAnswers();
     for (int i = 0; i < answers.count(); i++)
     {
         str.append(QString("<p>") + QString::number(i+1) + QString(". ") + answers.at(i) + QString("</p>"));
     }
     return str;
}
//показ гуи для текущего вопроса
void MainWindow::createTaskGui()
{
    hideTestGui();
    QString htmlOutput = createHtml();
    ui->taskPlainText->setText(htmlOutput);
    //m_questLabel->setText(m_tasks.at(currentTask).getQuestion());

    QList<QString> answers = m_tasks.at(currentTask).getAnswers();
    for (int i = 0; i < answers.count(); i++)
    {
        if (i == ANS_COUNT) break;
        m_answersList.at(i)->setText(QString::number(i+1));
        m_answersList.at(i)->show();
    }


    if(currentTask == m_tasks.count()-1)
    {
        m_nextButton->setDisabled(false);
        m_prevButton->setDisabled(false);
        m_prevButton->setText("Предыдущий");
        m_nextButton->setText("Завершить");
    }
    else if(currentTask == 0)
    {
        m_nextButton->setDisabled(false);
        m_nextButton->setText("Следующий");
    }
    else
    {
        m_nextButton->setDisabled(false);
        m_prevButton->setDisabled(false);
        m_prevButton->setText("Предыдущий");
        m_nextButton->setText("Следующий");
    }
    if(currentTask == 0)
        m_prevButton->setDisabled(true);
}

//получение номера выбранного ответа
int MainWindow::getAnswerId()
{

    for(int i = 0; i < m_answersList.count(); i++)
    {
        if(m_answersList.at(i)->isChecked() && !m_answersList.at(i)->isHidden())
            return i;
    }
    return -1;
}

//загрузка предыдущего вопроса
void MainWindow::prevTaskLoad()
{
    if(currentTask > 0)
        currentTask--;
    createTaskGui();
}

//загрузка следующего вопроса/завершение теста
void MainWindow::nextTaskLoad()
{
    if(getAnswerId() != -1)
        checkAnswerCorrectness();

    QList<int> unanswered = m_correctness.keys();
    if(currentTask == m_tasks.count()-1)
    {
       if(unanswered.count() < m_tasks.count())
       {
           QMessageBox::information(this, "Информация",
                                    QString("%1 unanswered questions").arg(m_tasks.count() - unanswered.count()));
       }
       else
       {
             m_log += getCurrentTimeF() + "Конец тестирования\r\n ******** \r\n";
             QFile fileOut("log.txt");
             if(fileOut.open(QIODevice::Append))
             {
                  QTextStream writeStream(&fileOut);
                  writeStream.setCodec(QTextCodec::codecForName("UTF-8"));
                  writeStream << m_log.toUtf8() ;
                  fileOut.close();
             }

             ResultsDialog *rDialog = new ResultsDialog(this);
             rDialog->setParams(m_studentName, m_correctness, m_selectedAnswers, m_tasks);
             rDialog->show();
             isTestStarted = false;
             setPage(1);
       }
    }
    else
    {        
        currentTask++;
        createTaskGui();
    }
}

//проверка правильности ответа
void MainWindow::checkAnswerCorrectness()
{    
    int id = getAnswerId();
    if (id < 0) return;

    m_log += getCurrentTimeF() + "Выбран ответ " + QString("№%1").arg(id+1) + " на вопрос " + QString("#%1").arg(currentTask+1) + "\r\n";

    int scores;
    switch(m_tasks.at(currentTask).getDifficulty())
    {
    case 1:
        scores = 2;
        break;
    case 2:
        scores = 3;
        break;
    case 3:
        scores = 5;
        break;
    }

    m_selectedAnswers[currentTask] = id;
    m_completenessBar->setValue(m_selectedAnswers.count());
    m_completenessBar->setFormat(QString("Ответов %1/%2").arg(m_completenessBar->value()).arg(TASK_COUNT));
    if (id == m_tasks.at(currentTask).getRightAnswer())
    {

        m_correctness[currentTask] = scores;
    }
    else
    {
        m_correctness[currentTask] = -1;
    }
}

void MainWindow::on_addTaskButton_clicked()
{
    TaskDialog *taskD = new TaskDialog();
    updateThemes();
    taskD->setParams(m_themes, this);
    taskD->show();
}

void MainWindow::setAllTasks(QList<Task> tasks)
{
    m_alltasks = tasks;
}

QList<Task> MainWindow::getAllTasks()
{
    return m_alltasks;
}

void MainWindow::setSecretChapterIsVisible()
{
    setPage(4);
    authorized = true;


    QFile file("lib/my.tests");
    if (!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::information(this, "Невозможно открыть файл с тестами", file.errorString());
        return;
    }
    else
    {
        testsDom.setContent(decodeStr(QString(file.readAll())));
        file.close();
    }
    m_themes.clear();
    m_alltasks.clear();
    QString question;
    QList<QString> answers;
    int rightAnswer;
    QString difficulty;

    QDomNodeList themesList = testsDom.elementsByTagName("theme");
    QDomNode themeNode;
    QString theme;
    QDomNodeList partsList;
    for (int i = 0; i < themesList.count(); i++)
    {

       theme = themesList.at(i).toElement().attribute("name");
       m_themes.append(theme);
       partsList = themesList.at(i).toElement().elementsByTagName("part");

       for (int j = 0; j < partsList.count(); j++)
       {
           answers.clear();
           QDomElement elem = partsList.at(j).toElement();
           QDomNodeList qAa = elem.childNodes();
           question = qAa.at(0).toElement().text();
           for (int k = 1; k < qAa.count(); k++)//because first was answer
           {
               if (qAa.at(k).toElement().attribute("correct").contains("true"))
                   rightAnswer = k-1;
               answers.append(qAa.at(k).toElement().text());
           }
           difficulty = elem.attribute("difficulty");

           Task newTask(question, answers, rightAnswer, difficulty, theme);
           m_alltasks.append(newTask);
       }
    }
    confTextUpdate();
}

void MainWindow::confTextUpdate()
{
    QString str;

    for (int i = 0; i < m_alltasks.count(); i++)
    {
        str.append(QString("<h3>Вопрос №") + QString::number(i+1) + QString("</h3>"));
        QString diff;
        switch(m_alltasks.at(i).getDifficulty())
        {
            case 1:
                diff = "Легко";
                break;
            case 2:
                diff = "Средне";
                break;
            case 3:
                diff = "Сложно";
                break;
        }
        str.append(QString("<h3>Тема: ") +  m_alltasks.at(i).getTheme()
                   + QString("   Сложность: ") +  diff + QString("</h3>"));
        str.append(QString("<p><i>") + m_alltasks.at(i).getQuestion() + QString("</i></p>"));
        QList<QString> answers = m_alltasks.at(i).getAnswers();
        for (int i = 0; i < answers.count(); i++)
        {
            if(i == 0)
                str.append(QString("<p><font color=green>") + QString::number(i+1) + QString(". ") + answers.at(i) + QString("</font></p>"));
            else
                str.append(QString("<p>") + QString::number(i+1) + QString(". ") + answers.at(i) + QString("</p>"));
        }
    }

    ui->confTextBrowser->setText(str);
}

void MainWindow::updateThemes()
{
    m_themes.clear();
    for (int i = 0; i < m_alltasks.count(); i++)
    {
        QString taskTheme = m_alltasks.at(i).getTheme();
        if (m_themes.indexOf(taskTheme) == -1)
            m_themes.append(taskTheme);
    }
}

void MainWindow::on_saveButton_clicked()
{   
    QDomDocument forSave("tests");
    QDomElement rootElem = forSave.createElement("tests");
    forSave.appendChild(rootElem);

    updateThemes();
    for (int i = 0; i < m_themes.count(); i++)
    {
        QDomElement themeElem = forSave.createElement("theme");
        themeElem.setAttribute("name", m_themes.at(i));
        rootElem.appendChild(themeElem);
    }

    QString diff;
    for (int i = 0; i < m_alltasks.count(); i++)
    {
        QDomElement partElem = forSave.createElement("part");
        switch(m_alltasks.at(i).getDifficulty())
        {
            case 1:
                diff = "easy";
                break;
            case 2:
                diff = "medium";
                break;
            case 3:
                diff = "difficult";
                break;
        }
        partElem.setAttribute("difficulty", diff);
        QDomElement questElem = forSave.createElement("question");
        QDomText questText = forSave.createTextNode(m_alltasks.at(i).getQuestion());
        questElem.appendChild(questText);
        partElem.appendChild(questElem);

        QList<QString> answers = m_alltasks.at(i).getAnswers();
        int rindex = m_alltasks.at(i).getRightAnswer();
        for(int a = 0; a < answers.count(); a++)
        {
            QDomElement ansElem = forSave.createElement("var");
            if (a == rindex)
            {
                ansElem.setAttribute("correct", "true");
            }
            else
            {
                ansElem.setAttribute("correct", "false");
            }
            QDomText ansText = forSave.createTextNode(answers.at(a));
            ansElem.appendChild(ansText);
            partElem.appendChild(ansElem);
        }

        QDomNodeList myThemes = forSave.elementsByTagName("theme");
        QDomElement currThemeElem;
        QString currTheme = m_alltasks.at(i).getTheme();
        for (int t = 0; t < myThemes.count(); t++)
        {
            if (myThemes.at(t).toElement().attribute("name").contains(currTheme))
            {
                currThemeElem = myThemes.at(t).toElement();
                currThemeElem.appendChild(partElem);
            }
        }

    }


    QFile file("./lib/my.tests");
    if (!file.open(QIODevice::WriteOnly))
    {
        QMessageBox::information(this, "Проблема сохранения", "Невозможно открыть файл с тестами", file.errorString());
        return;
    }
    else
    {
        QTextStream writeStream(&file);
        writeStream << encodeStr(forSave.toString());
        file.close();
        QMessageBox::information(this, "Информация", "Успешно сохранено");
        confTextUpdate();
    }

}

void MainWindow::on_editTaskButton_clicked()
{
    if(m_alltasks.count() < 1)
    {
      QMessageBox::information(this, "Информация", "Сначала создайте хотя бы один вопрос");
      return;
    }

    TaskDialog *taskD = new TaskDialog();
    updateThemes();
    taskD->isEdit = true;
    taskD->setParams(m_themes, this);
    taskD->show();
}

void MainWindow::on_deleteTaskButton_clicked()
{
    if(m_alltasks.count() < 1)
    {
      QMessageBox::information(this, "Информация", "Сначала создайте хотя бы один вопрос");
      return;
    }

    TaskDialog *taskD = new TaskDialog();
    updateThemes();
    taskD->isDelete = true;
    taskD->setParams(m_themes, this);
    taskD->show();
}

QString MainWindow::encodeStr(const QString& str)
{
    QByteArray arr(str.toUtf8());
    for(int i =0; i<arr.size(); i++)
        arr[i] = arr[i] ^ key;

    return QString::fromUtf8(arr.toBase64());
}

QString MainWindow::decodeStr(const QString &str)
{
    QByteArray arr = QByteArray::fromBase64(str.toUtf8());
    for(int i =0; i<arr.size(); i++)
        arr[i] =arr[i] ^ key;

    return QString::fromUtf8(arr);
}


