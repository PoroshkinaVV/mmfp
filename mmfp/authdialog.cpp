#include "authdialog.h"
#include "ui_authdialog.h"
#include <QDebug>

QString PASSWORD = "tm321";

authdialog::authdialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::authdialog)
{
    ui->setupUi(this);

}

authdialog::~authdialog()
{
    delete ui;
}

void authdialog::on_pushButton_clicked()
{
    if (isTestLogon)
    {
        QString name = ui->lineEdit->text();
        if (name.isEmpty())
        {
            QMessageBox::warning(this,"Авторизация", "Пустое имя");
        }
        else
        {
            m_main->setStudentName(name);
            emit showTest();
            close();
        }
    }
    else
    {
        QString password = ui->lineEdit->text();
        if(password == PASSWORD) {
            emit showSecretChapter();
            close();
        }
        else {
            QMessageBox::warning(this,"Авторизация", "Пароль не верен.");
        }
    }

}

void authdialog::setStatusTest(MainWindow *parent)
{
    isTestLogon = true;
    m_main = parent;
    ui->label->setText("Фамилия студента");
    ui->lineEdit->setEchoMode(QLineEdit::Normal);
}
