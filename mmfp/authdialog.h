#ifndef AUTHDIALOG_H
#define AUTHDIALOG_H

#include <QDialog>
#include <QMessageBox>
#include "mainwindow.h"

class MainWindow;

namespace Ui {
class authdialog;
}

class authdialog : public QDialog
{
    Q_OBJECT

public:
    explicit authdialog(QWidget *parent = 0);
    ~authdialog();

    void setStatusTest(MainWindow *parent);


private slots:
    void on_pushButton_clicked();

private:
    Ui::authdialog *ui;
    bool isTestLogon;
    MainWindow *m_main;

signals:
    void showSecretChapter();
    void showTest();
};

#endif // AUTHDIALOG_H
