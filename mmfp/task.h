#ifndef TASK_H
#define TASK_H

#include <QString>
#include <QList>

class Task
{
public:
    Task();
    Task(QString _question, QList<QString> _answers, int _rightAnswer, QString _difficulty, QString _theme);
    ~Task();

    QString getQuestion() const;
    QList<QString> getAnswers() const;
    int getRightAnswer() const;
    int getDifficulty() const;
    QString getTheme() const;
private:
    int rightAnswer;
    QString question;
    QList<QString> answers;
    int difficulty; //1-easy, 2-medium, 3-difficult
    QString theme;

};

#endif // TASK_H
